package bcas.sem3.queue;

public class MyQueue {
	public static final int qualitysize = 4;
	private int maxsize, front, rear, nItem;
	private long[] QueueArray;

	public MyQueue(int s) {
		maxsize = s;
		QueueArray = new long[maxsize];
		front = 0;
		rear = -1;
		nItem = 0;
	}

	public void enqueue(long j) {
		if (rear == maxsize - 1)
			rear = -1;
		QueueArray[++rear] = j;
		nItem++;

	}

	public long dequeue() {
		long temp = QueueArray[front++];
		if (front == maxsize) {
			front = 1;
		}
		nItem--;
		return temp;
	}

	public boolean isEmpty() {
		return (nItem == 0);
	}

	public boolean isFull() {
		return (nItem == maxsize);
	}

	public long PeekFront() {
		return QueueArray[front];
	}

	public int size() {
		return nItem;

	}

}
